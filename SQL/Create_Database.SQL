/*

--Altera a database para Offline, possibilitando a execução do drop. A execução desta função via shell do Management Studio Ainda não funciona no SQL Server 2016 RC1.
--ALTER DATABASE METRIC SET OFFLINE 
GO

--DROP DATABASE METRIC
GO


--Execução de comandos via shell de CMD, solicitando ao SO que remova os arquivos de base e log.



enable xp_cmdshell

EXEC sp_configure 'show advanced options', 1
GO
RECONFIGURE
GO
EXEC sp_configure'xp_cmdshell', 1  
GO

--Deleta os arquivos de base e Log do sistema.
EXEC xp_cmdshell 'DEL "C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\METRIC.mdf"'
GO
EXEC xp_cmdshell 'DEL "C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\METRIC_log.LDF"'
GO

-- Exibe o caminho fisico de cada arquivo atualmente anexado ao MSDB
SELECT name, physical_name AS file_path
FROM sys.master_files

*/

CREATE DATABASE METRIC
GO

USE METRIC
GO


CREATE TABLE Caracteristicas(
id_Caracteristicas INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
desc_Caracteristicas CHAR (255),
peso_Caracteristicas INT NOT NULL
)
GO

ALTER TABLE Caracteristicas ADD CONSTRAINT Uniq_Cat UNIQUE (desc_Caracteristicas) --Cria verificação de dado unico na coluna desc_Caracteristicas.
GO

--Cria tabela de SubCaracteristicas
CREATE TABLE SubCaracteristicas(
id_SubCaracteristica INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
desc_SubCaracteristica CHAR (255),
peso_SubCaracteristica INT NOT NULL,
id_Caracteristicas INT FOREIGN KEY REFERENCES Caracteristicas(id_Caracteristicas)

)
GO

ALTER TABLE SubCaracteristicas ADD CONSTRAINT Uniq_sCat UNIQUE (desc_SubCaracteristica) --Cria verificação de dado unico na coluna desc_SubCaracteristica.


--Cadastra as perguntas chaves relacionadas as sub-caracteristicas
CREATE TABLE PerguntaChave (
id_Pergunta INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
desc_Pergunta CHAR(255) NULL,
id_SubCaracteristica INT FOREIGN KEY REFERENCES SubCaracteristicas(id_SubCaracteristica)
)

--Cria tabela de cadastro de sistemas
CREATE TABLE Sistemas (
id_Sistema INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
desc_Sistema CHAR(255) NOT NULL,
dt_Cadastro DATETIME DEFAULT GETDATE()
)
GO

ALTER TABLE Sistemas ADD CONSTRAINT Uniq_sis UNIQUE (desc_Sistema)--Cria verificação de dado unico na coluna desc_Sistema.
GO


--Cria tabela de registro de parametrizações
CREATE TABLE Avaliacao (
id_Avaliacao INT NOT NULL IDENTITY(1,1) PRIMARY KEY,

id_Sistema INT FOREIGN KEY REFERENCES Sistemas(Id_Sistema),
id_Caracteristicas INT FOREIGN KEY REFERENCES Caracteristicas(id_Caracteristicas),
id_SubCaracteristica INT FOREIGN KEY REFERENCES SubCaracteristicas(id_SubCaracteristica),
dt_Cadastro DATE DEFAULT GETDATE(),
--dt_UltAlteracao DATETIME DEFAULT GETDATE(), existe a necessidade de alterar algo cadastrado?
--IP	   CHAR (15),
[host_name] CHAR(255) DEFAULT HOST_NAME(),
Nota REAL NOT NULL
)
GO



INSERT INTO Caracteristicas ( desc_Caracteristicas, peso_Caracteristicas   )
VALUES ('Funcionalidade', 0), ('Confiabilidade', 0),  ('Usabilidade', 0), ('Eficiência', 0), ('Manutenibilidade', 0), ('Portabilidade',0)
GO


INSERT INTO SubCaracteristicas (desc_SubCaracteristica, peso_SubCaracteristica, id_Caracteristicas )
VALUES ('Adequação', 0, 1), ('Acurácia', 0, 1), ('Interoperabilidade', 0, 1), ('Conformidade', 0, 1), ('Segurança de Acesso', 0, 1),
	   ('Maturidade', 0, 2), ('Tolerância a Falhas', 0, 2), ('Recuperabilidade', 0, 2),
	   ('Integebilidade', 0, 3), ('Apreensibilidade', 0, 3), ('Operacionabilidade', 0, 3),
	   ('Tempo', 0, 4), ('Recursos', 0, 4),
	   ('Analibilidade', 0, 5), ('Modificabilidade', 0, 5), ('Estabilidade', 0, 5), ('Testabilidade', 0, 5),
	   ('Adaptabilidade', 0, 6),('Capacidade para de ser instalado', 0, 6), ('Em Conformidade', 0, 6), ('Capacidade para substituir', 0, 6)

GO


INSERT INTO PerguntaChave (desc_Pergunta, id_SubCaracteristica)
VALUES ('Propõe-se a fazer o que é apropriado?', 1 ), ('Faz o que foi proposto de forma correta?', 1), ('Interage com os sistemas especificados?', 1 ), ('Está de acordo com as normas, leis, etc.?', 1), ('Evita acesso não autorizado aos dados?', 1),
	   ('Com que frequência apresenta falhas?', 2), ('Ocorrendo falhas, como ele reage?', 2), ('É capaz de recuperar dados em caso de falhas?', 2),
	   ('É fácil entender o conceito e a aplicação?', 3),('É fácil aprender a usar?', 3),('É fácil de operar e controlar?', 3),
	   ('Quanto ao tempo de resposta e a velocidade de execução?', 4), ('Referente a quantidade de recursos utilizados e tempo?', 4),
	   ('É fácil encontrar uma falha, quando ocorre?', 5), ('É fácil de modificar e adptar?', 5),('Há grandes riscos quando se faz alterações?', 5),('É fácil testar quando se faz alterações?', 5),
	   ('É fácil de adptar as outros ambientes?', 6), ('É fácil instalar em outros ambientes?', 6),('Está de acordo com os padrões de portabilidade?', 6),('É fácil usar para substituir outro?', 6)


--Criar procedure permitindo alterar os pesos como variaveis, falar gu/caique
--Aplicar validações de valores informadas pelo caique/ver com gu se nao é mais fácil fazer no java.
--Ordernadar ordem de criacao das tabelas.
--Dar rollback no id caso a constraint bloqueie a transacao.
--Ajustar identity, talvez com NEWID() ou IDENTITY antigo +1 = novo identity (ideia para resolver conflitos com rollback de transacao