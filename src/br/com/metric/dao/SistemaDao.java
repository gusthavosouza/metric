package br.com.metric.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import br.com.metric.factory.ConnectionFactory;
import br.com.metric.interfaces.IDao;
import br.com.metric.model.Caracteristicas;
import br.com.metric.model.Sistema;
import br.com.metric.model.SubCaracteristicas;
import br.com.metric.utils.Messages;

public class SistemaDao implements IDao {

	@Override
	public boolean remover(int id) {

		try {
			
		} catch (Exception e) {
			
		} finally {
			
		}
		return false;
	}

	@Override
	public boolean alterar(Object obj) {
		try {
			
		} catch (Exception e) {
			
		} finally {
			
		}
		return false;
	}

	@Override
	public boolean salvar(Object obj) {
		return false;
	}

	@Override
	public List<?> listar() {
		
		List<Sistema> listSistemas = new ArrayList<Sistema>();
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			
			conn = ConnectionFactory.open();
			pstm = conn.prepareStatement("SELECT * FROM SISTEMAS;");
			rs = pstm.executeQuery();
			
			while (rs.next()) {
				Sistema sistema = new Sistema();
				sistema.setId(rs.getInt("id_sistema"));
				sistema.setNome(rs.getString("desc_sistema"));
				listSistemas.add(sistema);
			}
			
		} catch (Exception  e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs, pstm, conn);
		}
		return listSistemas;
	}

	@Override
	public List<?> listar(Object obj) {
		
		List<Sistema> listSistemas = null;
		try {
			
		} catch (Exception  e) {
			
		} finally {
			
		}
		return listSistemas;
	}
	
	public boolean salvar (Sistema sistema, List<Caracteristicas> caracteristicas) {
		
		Connection conn = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		
		try {
			conn = ConnectionFactory.open();
			pstm = conn.prepareStatement("INSERT INTO SISTEMAS (DESC_SISTEMA) VALUES (?) RETURNING id_sistema");
			pstm.setString(1, sistema.getNome());
			rs = pstm.executeQuery();
			
			int id = 0;
			
			if (rs.next())
				id = rs.getInt("id_sistema");
			
			pstm2 = conn.prepareStatement("INSERT INTO avaliacao(id_sistema, id_caracteristicas, id_subcaracteristica, nota)"
					+ " VALUES (?, ?, ?, ?);");
			
			for (Caracteristicas car : caracteristicas) {
//				pstm2.setInt(1, id);
//				pstm2.setInt(2, car.getIdCaracteristicas());
//				pstm2.setNull(3, Types.NULL);
//				pstm2.setDouble(4, car.getNotaCaracteristica());
//				pstm2.execute();
				
				if (car.getSubCaracteristicas() == null || car.getSubCaracteristicas().isEmpty())
					continue;
				
				List<SubCaracteristicas> subs = car.getSubCaracteristicas();
				
				for (SubCaracteristicas sub : subs) {
					
					pstm2.setInt(1, id);
					pstm2.setInt(2, car.getIdCaracteristicas());
					pstm2.setInt(3, sub.getIdSubCaracteristica());
					
					if (sub.getNotaSubcaracteristica() != null)
						pstm2.setDouble(4, sub.getNotaSubcaracteristica());
					else
						pstm2.setDouble(4, 0.0d);

					pstm2.execute();
					
				}
			}
			conn.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getMessage().contains("uniq_sis"))
				Messages.exibeMensagemErro("Um Sistema com o nome "+sistema.getNome()+" j� foi cadastrado anteriormente.");
			try {conn.rollback();} catch (Exception e1) {}
		} finally {
			ConnectionFactory.close(rs, pstm, pstm2, conn);
		}
		return false;
	}

}